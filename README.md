# ansible_provision_k8s

## Description:
This project demonstrates how to use Ansible to spin up a 1-master-2-node Kubernetes cluster on top of 3 newly-provisioned ec2 instances.

## Project Structure:
```
.
├── README.md
├── ansible.cfg
├── k8s-ansible # private key used for accessing newly-provisioned ec2 instances
├── full_playbook.yaml # playbook to spin up the whole cluster
├── master_playbook.yaml # playbook to spin up just the master
├── node_playbook.yaml # playbook to spin up just the nodes
├── debug_playbook.yaml # playbook to terminate all ec2 which are just provisioned
├── inventories
│   ├── group_vars
│   └── staging_inventory.aws_ec2.yaml
└── roles
    ├── InitK8s # prepare newly-provisioned ec2 instances for both k8s master and node
    ├── K8sMaster
    ├── K8sNode
    └── Provision # provision ec2 instances
```

## Usage:
1. Install and configure AWS CLI as instructed [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html).
2. Install Ansible as instructed [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
3. Change permission of the `k8s-ansible` to 400.
4. Generate the corresponding public key of the `k8s-ansible` key and upload the public key to aws (*Make sure to name the key pair `k8s-ansible`).
5. Set the `AWS_ACCESS_KEY_ID` `AWS_SECRET_ACCESS_KEY` environment variable.
6. Run `ansible-playbook full_playbook.yaml`. If Ansible reports missing python libraries (usually boto, botocore, boto3), install them and run again.
7. (Optional) Run `ansible-playbook debug_playbook` to termiate all newly-provisioned ec2 instances.

## Advanced Usage:
The project uses a number of variable which can be modified, including:
1. The number of k8s nodes - `ec_count` in `full_playbook.yaml` and `node.yaml`.
2. Various ec2 attributes like the key pair to use, image, instance type. These values can be modified in `roles/Provision/vars/main.yaml`.