#!/bin/bash
# Install Calico pod network plugin, following instructions at https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/
kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml