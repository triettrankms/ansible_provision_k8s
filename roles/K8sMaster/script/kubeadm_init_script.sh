#!/bin/bash
#Config k8s for non-root users, following instructions at https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config